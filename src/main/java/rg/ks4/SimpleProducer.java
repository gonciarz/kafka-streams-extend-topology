package rg.ks4;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

class SimpleProducer {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        new SimpleProducer().run();
    }

    public void run() throws ExecutionException, InterruptedException {

        try (var producer = createProducer(createConfig())) {
            var key = UUID.randomUUID().toString();
            var value = "foo";
            producer.send(new ProducerRecord<>("input-topic", key, value)).get();
        }

    }

    private KafkaProducer<String, String> createProducer(Map<String, Object> config) {
        return new KafkaProducer<>(config);
    }

    private Map<String, Object> createConfig() {
        return Map.of(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092",
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class,
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class
        );
    }


}


