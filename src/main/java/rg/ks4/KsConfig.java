package rg.ks4;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.config.KafkaStreamsInfrastructureCustomizer;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;

import java.util.function.Function;

@Slf4j
@EnableKafkaStreams
@Configuration
public class KsConfig {

    @Bean
    public Function<KStream<String, String>, KStream<String, String>> process() {
        return input -> input
                .peek(((key, value) -> log.info("({},{})", key, value)))
                .mapValues((key, value) -> value.toUpperCase());
    }

    @Bean
    public KafkaStreamsInfrastructureCustomizer kafkaStreamsInfrastructureCustomizer() {
        return new KafkaStreamsInfrastructureCustomizer() {

            @Override
            public void configureTopology(Topology topology) {
                log.info("==== TOPOLOGY");
                topology.describe().subtopologies().stream()
                        .flatMap(subtopology -> {
                            log.info("Subtopology: {}", subtopology.id());
                            return subtopology.nodes().stream();
                        })
                        .forEach(node -> {
                            log.info("Node: {}", node.name());

                            log.info("Pre:");
                            node.predecessors().forEach(pre -> log.info("{}", pre.name()));

                            log.info("Suc");
                            node.successors().forEach(suc -> log.info("{}", suc.name()));
                        });
            }
        };

    }

    /*
    @Bean
    public KafkaStreamsConfiguration defaultKafkaStreamsConfig() {
        Map<String, Object> config = Map.of(
                StreamsConfig.APPLICATION_ID_CONFIG, "barId",
                StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092"
        );
        return new KafkaStreamsConfiguration(config);
    }
    */

    @Bean
    public FactoryBean<StreamsBuilder> streamsBuilderFactoryBean(
            KafkaStreamsConfiguration defaultKafkaStreamsConfig,
            KafkaStreamsInfrastructureCustomizer kafkaStreamsInfrastructureCustomizer) {
        var factoryBean = new StreamsBuilderFactoryBean(defaultKafkaStreamsConfig);
        factoryBean.setInfrastructureCustomizer(kafkaStreamsInfrastructureCustomizer);
        return factoryBean;
    }

}
