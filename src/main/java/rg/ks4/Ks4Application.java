package rg.ks4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ks4Application {

	public static void main(String[] args) {
		SpringApplication.run(Ks4Application.class, args);
	}

}
